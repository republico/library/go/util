package string

import (
    "unicode/utf8"
)

func RemoveInvalidUtfChar(data string) string {
	response := data

	if !utf8.ValidString(data) {
		v := make([]rune, 0, len(data))
		for i, r := range data {
			if r == utf8.RuneError {
				_, size := utf8.DecodeRuneInString(data[i:])
				if size == 1 {
					continue
				}
			}
			v = append(v, r)
		}
		response = string(v)
	}

	return response
}