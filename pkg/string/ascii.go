package string

import (
    "regexp"
)

func RemoveInvalidAsciiChar(data string) string {
	re := regexp.MustCompile("[[:^ascii:]]")
	response := re.ReplaceAllLiteralString(data, "")

	return response
}