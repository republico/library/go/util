package convert

func AdjustSeparator(separator string) int32 {
	separatorAdjusted := ';'

	if separator == ";" {
		separatorAdjusted = ';'
	} else if separator == "," {
		separatorAdjusted = ','
	} else if separator == "	" {
		separatorAdjusted = '	'
	}

	return separatorAdjusted
}