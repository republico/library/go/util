package url

import (
    "net/http"
    "time"
)

func Exists(path string) (bool, error) {
    timeout := time.Duration(5 * time.Second)
	client := http.Client{
	    Timeout: timeout,
	}

    _, err := client.Get(path)
	if err != nil {
		return false, err
    }

    return true, err
}