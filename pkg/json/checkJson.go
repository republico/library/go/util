package json

import (
    "encoding/json"
)

func IsJsonString(s string) bool {
    var js string
    return json.Unmarshal([]byte(s), &js) == nil
}

func IsJson(s string) bool {
    var js interface{}
    return json.Unmarshal([]byte(s), &js) == nil
}