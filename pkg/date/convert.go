package date

func ConvertMounthLettersInNumber(mounth string) string {
	result := ""

	switch mounth {
	case "Jan":
		result = "01"
	case "Feb":
		result = "02"
	case "Mar":
		result = "03"
	case "Apr":
		result = "04"
	case "May":
		result = "05"
	case "Jun":
		result = "06"
	case "Jul":
		result = "07"
	case "Aug":
		result = "08"
	case "Sep":
		result = "09"
	case "Oct":
		result = "10"
	case "Nov":
		result = "11"
	case "Dec":
		result = "12"
	}
	
	return result
}