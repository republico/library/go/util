package minify

import (
	"bytes"
	
	"github.com/tdewolff/minify"
	"github.com/tdewolff/minify/json"
)

func MinifyStringJson(data string) (string, error) {
	m := minify.New()
	w := &bytes.Buffer{}
	r := bytes.NewBufferString(data)

	err := json.Minify(m, w, r, nil)
	if err != nil {
		return "", err
	}

	return w.String(), nil
}