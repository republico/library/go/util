package file

import (
    "os"
)

func CreateFile(path string) error {
	flagExists, _ := Exists(path)
	if !flagExists {
		file, err := os.Create(path)
		if err != nil {
			return err
		}
		defer file.Close()
	}

	return nil
}

func CreateDir(path string) error {
	flagExists, _ := Exists(path)
	if !flagExists {
		if err := os.MkdirAll(path, os.ModePerm); err != nil {
			return err
		}
	}

	return nil
}