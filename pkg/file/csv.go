package file

import (
    "bufio"
	"encoding/csv"
	"io"
	"os"

	csvUtil "gitlab.com/republico/library/go/util/pkg/csv"
)

func ReadHeaderCsv(csvFilePath string, separator string) ([]string, error) {
	separatorAdjusted := csvUtil.AdjustSeparator(separator)

	csvFile, err := os.Open(csvFilePath)
	if err != nil {
		return []string{}, err
	}
	defer csvFile.Close()

	buffer := bufio.NewReader(csvFile)
	reader := csv.NewReader(buffer)
	reader.Comma = separatorAdjusted

    for {
        line, err := reader.Read()
        if err != nil {
			return []string{}, err
		}

		return line, nil
	}
}

func ReadDataCsv(csvFilePath string, separator string) ([][]string, error) {
	separatorAdjusted := csvUtil.AdjustSeparator(separator)

	csvFile, err := os.Open(csvFilePath)
	if err != nil {
		return [][]string{}, err
	}
	defer csvFile.Close()

	buffer := bufio.NewReader(csvFile)
	reader := csv.NewReader(buffer)
	reader.Comma = separatorAdjusted

	var csvData [][]string

    for {
        line, err := reader.Read()
        if err == io.EOF {
            break
        } else if err != nil {
            if err != nil {
				return [][]string{}, err
			}
		}

		csvData = append(csvData, line)
	}

	csvData = csvData[1:]

	return csvData, nil
}

func ReadCsv(csvFilePath string, separator string) ([][]string, error) {
	separatorAdjusted := csvUtil.AdjustSeparator(separator)

	csvFile, err := os.Open(csvFilePath)
	if err != nil {
		return [][]string{}, err
	}
	defer csvFile.Close()

	buffer := bufio.NewReader(csvFile)
	reader := csv.NewReader(buffer)
	reader.Comma = separatorAdjusted

	var csvData [][]string

    for {
        line, err := reader.Read()
        if err == io.EOF {
            break
        } else if err != nil {
            if err != nil {
				return [][]string{}, err
			}
		}

		csvData = append(csvData, line)
	}

	return csvData, nil
}