package file

import (
    "os"
)

func IsDirectory(path string) (bool, error) {
    fileInfo, err := os.Stat(path)
    if err != nil{
      return false, err
    }

    return fileInfo.IsDir(), err
}

func IsFile(path string) (bool, error) {
    fileInfo, err := os.Stat(path)
    if err != nil{
      return false, err
    }

    return !fileInfo.IsDir(), err
}

func Exists(path string) (bool, error) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false, err
	} else {
		return true, err
	}
}