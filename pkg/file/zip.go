package file

import (
    "archive/zip"
    "errors"
    "io"
    "os"
    "path/filepath"
    "strings"
)

func Unzip(srcFilePath string, destDirPath string) error {
    var filenames []string

    zipFile, err := zip.OpenReader(srcFilePath)
    if err != nil {
		return err
	}
    defer zipFile.Close()

    for _, f := range zipFile.File {
        rc, err := f.Open()
		if err != nil {
            return err
        }
        defer rc.Close()

        fpath := filepath.Join(destDirPath, f.Name)
        if !strings.HasPrefix(fpath, filepath.Clean(destDirPath) + string(os.PathSeparator)) {
            return errors.New("Endereço ilegal")
        }

        filenames = append(filenames, fpath)
        if f.FileInfo().IsDir() {
            os.MkdirAll(fpath, os.ModePerm)
        } else {
            err = os.MkdirAll(filepath.Dir(fpath), os.ModePerm)
			if err != nil {
                return err
            }

            outFile, err := os.OpenFile(fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
			if err != nil {
                return err
            }

            _, err = io.Copy(outFile, rc)
            outFile.Close()
			if err != nil {
                return err
            }
        }
    }

    return nil
}