
package file

import (
	"strings"
)

func GetExtension(dataType string) string {
    split := strings.Split(dataType, ".")
    extension := split[len(split) - 1]
    
    return extension
}