package exception

import (
    "log"
)

func HandlerError(err error) {
    if err != nil {
        log.Fatal(err)
        panic(err)
    }
}

func PrintError(msg string) {
    log.Println(msg)
}