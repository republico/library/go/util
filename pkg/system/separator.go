package system

import (
    "runtime"
)

func GetSeparator() string {
	separator := "/"

	if runtime.GOOS == "windows" {
		separator = "\\"
	}

	return separator
}