package cmd

import (
	"fmt"
    "os/exec"
	"strings"
)

func Execute(command string) (string, error) {
	fields := strings.Fields(command)
	head := fields[0]
	parts := fields[1 : len(fields)]

	cmd := exec.Command(head, parts...)
	out, err := cmd.CombinedOutput()

	response := fmt.Sprintf("%s", out)

	return response, err
}