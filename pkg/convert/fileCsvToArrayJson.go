package convert

import (
    "bufio"
	"encoding/csv"
	"io"
	"os"
	"strings"

	"github.com/dimchansky/utfbom"

	csvUtil "gitlab.com/republico/library/go/util/pkg/csv"
)

func FileCsvToArrayJson(csvFilePath string, separator string) (string, error) {
	csvFile, err := os.Open(csvFilePath)
	if err != nil {
		return "", err
	}
    defer csvFile.Close()

	buffer := bufio.NewReader(csvFile)
	data, _ := utfbom.Skip(buffer)
	reader := csv.NewReader(data)

	separatorAdjusted := csvUtil.AdjustSeparator(separator)
	reader.Comma = separatorAdjusted

	flagHeader := true
	var header []string

	jsonArray := "["
    for {
        line, err := reader.Read()
        if err == io.EOF {
            break
        } else if err != nil {
            if err != nil {
				return "", err
			}
		}

		if flagHeader {
			header = line
			flagHeader = false
		} else {
			json := LineCsvToJson(line, header)
			jsonArray = jsonArray + json + ","
		}
	}
	jsonArray = strings.TrimRight(jsonArray, ",")
	jsonArray = jsonArray + "]"

	return jsonArray, nil
}