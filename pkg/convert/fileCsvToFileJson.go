package convert

import (
    "bufio"
	"encoding/csv"
	"io"
	"io/ioutil"
	"os"
	"strings"

	"github.com/dimchansky/utfbom"

	csvUtil "gitlab.com/republico/library/go/util/pkg/csv"
)

func FileCsvToFileJson(csvFilePath string, destFilePath string, separator string) error {
	separatorAdjusted := csvUtil.AdjustSeparator(separator)

	csvFile, err := os.Open(csvFilePath)
	if err != nil {
		return err
	}
    defer csvFile.Close()

	buffer := bufio.NewReader(csvFile)
	data, _ := utfbom.Skip(buffer)

	reader := csv.NewReader(data)
	reader.Comma = separatorAdjusted

	flagHeader := true
	var header []string

	jsonArray := "["
    for {
        line, err := reader.Read()
        if err == io.EOF {
            break
        } else if err != nil {
            if err != nil {
				return err
			}
		}

		if flagHeader {
			header = line
			flagHeader = false
		} else {
			json := LineCsvToJson(line, header)
			jsonArray = jsonArray + json + ","
		}
	}
	jsonArray = strings.TrimRight(jsonArray, ",")
	jsonArray = jsonArray + "]"

	dataBytes := []byte(jsonArray)
	ioutil.WriteFile(destFilePath, dataBytes, 0)

	return nil
}