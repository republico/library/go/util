package convert

import (
	"strings"
)

func LineCsvToJson(line []string, header []string) string {
	json := "{"
	for i := 0; i < len(header); i++ {
		json = json + "\"" + header[i] + "\":" + "\"" + line[i] + "\","
	}
	json = strings.TrimRight(json, ",")
	json = json + "}"

	return json
}