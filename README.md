# Util

## Parâmetros

Lista de parâmetros a serem passados na linha de comando:

* Parâmetro 1 = Nome do serviço
* Parâmetro 2 = Nome do arquivo
* Parâmetro * = Depende do serviço desejado

## Comandos

### Teste

```
go run ./*.go unzip C:/consorcios.zip C:/Teste
go run ./*.go convertfile C:/Teste/consorcios.csv C:/Teste/consorcios.json csv json
go run ./*.go csvheader C:/Teste/consorcios.csv
```

#### Testes Unitários

```
go test .\test\cmd -v -run TestExecuteCommand
go test .\test\string -v -run TestRemoveInvalidAsciiChar
go test .\test\string -v -run TestRemoveInvalidUtfChar
```