package test

import (
	"testing"

	"gitlab.com/republico/library/go/util/pkg/cmd"
)

func TestExecuteCommand(t *testing.T) {
	command := "ls"

	response, err := cmd.Execute(command)

	if err != nil {
		t.Errorf("Uma exception foi lançada.")
	} else if len(response) == 0 {
		t.Errorf("Ocorreu um erro.")
	}
}