package test

import (
	"fmt"
	"testing"

	utilString "gitlab.com/republico/library/go/util/pkg/string"
)

func TestRemoveInvalidUtfChar(t *testing.T) {
	data := "Teste: 1 \xc52 ï»¿3"

	response := utilString.RemoveInvalidUtfChar(data)

	if len(response) == 0 {
		t.Errorf("Ocorreu um erro.")
	} else {
		fmt.Println(response)
	}
}